var star_canv = document.getElementById("Stars");
var star_ctx = star_canv.getContext("2d");


star_canv.width = window.innerWidth;
star_canv.height = window.innerHeight;

star_ctx.strokeStyle = 'rgba(0,200,255,'+ (Math.random() * (1.0 - 0.3) + 0.3) +')';
star_ctx.lineCap = 'round';

var particles = [];
var MaxParticles = 500;

initilization();
draw();

function initilization(){

    for(var i = 0; i < MaxParticles; i++){
        particles.push({
            x: Math.random() * star_canv.width,
            y: Math.random() * star_canv.height,
            speed_x: Math.random() * 5 + 1,
            speed_y: -4 + Math.random() * 5 + 1,
            size: Math.random() * 2 + 1
        });
    }
}

async function draw(){
    star_ctx.clearRect(0, 0, star_canv.width, star_canv.height);
    
    if(window.innerWidth != star_canv.width){
        star_canv.width = window.innerWidth;
        star_canv.height = window.innerHeight;
        
        star_ctx.strokeStyle = 'rgba(0,200,255,'+ (Math.random() * (1.0 - 0.3) + 0.3) +')';
    }

    for(var i = 0; i < MaxParticles; i++) {
        if(particles[i].x <= star_canv.width) {
            particles[i].x += particles[i].speed_x / 20;
            particles[i].y += particles[i].speed_y / 20;
        }
        else {
            particles[i].x = 0;
            particles[i].y = Math.random() * star_canv.height;
        }
        
    }

    if(star_canv.width > 720)
        for(var i = 0; i < MaxParticles; i++){
            star_ctx.beginPath();
            star_ctx.arc(particles[i].x, particles[i].y, particles[i].size, 0, 2 * Math.PI, false);
    
            star_ctx.stroke();
        }

    requestAnimationFrame(draw);
}

