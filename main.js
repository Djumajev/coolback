const electron = require('electron');
const { exec } = require('child_process');
const { app, BrowserWindow } = require('electron');
const path = require('path');

const MAIN_HTML = path.join('file://', __dirname, 'main.html');
const CHILD_PADDING = 50;

const onAppReady = function () {

  exec("wmctrl -k on");

  let parent = new BrowserWindow({
    width: 300,
    height: 300,
    transparent: true,
    frame: false,
    skipTaskbar: true,
  });

  parent.once('close', () => {
    parent = null;
  });

  parent.maximize();

  parent.loadURL(MAIN_HTML);

  let displays = electron.screen.getAllDisplays()
  let externalDisplay = displays.find((display) => {
    return display.bounds.x !== 0 || display.bounds.y !== 0
  })

  if (externalDisplay) {
    win = new BrowserWindow({
      x: externalDisplay.bounds.x + 50,
      y: externalDisplay.bounds.y + 50,
      transparent: true,
      frame: false,
      skipTaskbar: true
    })

    win.maximize();

    win.once('close', () => {
      win = null;
    });

    win.loadURL(MAIN_HTML);
  }

};

//~ app.on('ready', onAppReady);
app.on('ready', () => setTimeout(onAppReady, 500));
